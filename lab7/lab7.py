from labs.lab6 import lab6


def diffie_hellman(d_a, d_b, G):
    curve = lab6.Curve()
    g_x, g_y = G
    key_1 = curve.exp(d_a, g_x, g_y)
    key_2 = curve.exp(d_b, g_x, g_y)
    return curve.exp(d_a, *key_2) == curve.exp(d_b, *key_1)


if __name__ == '__main__':
    d_a = 908321
    d_b = 456789
    G = (908321, 45645611)
    print(diffie_hellman(d_a, d_b, G))
