from skimage import io
from skimage.util import view_as_blocks
from scipy.fftpack import dct, idct
import numpy as np


abs_plus = lambda x: x + 1 if x >= 0 else x - 1

abs_minus = lambda x: 0 if np.abs(x) <= 1 else x - 1 if x >= 0 else x + 1


def update_coeff(block, bit):
    temp = block.copy()
    if bit == 0:
        temp[1, 3] = abs_plus(temp[1, 3])
        temp[3, 1] = abs_minus(temp[3, 1])
    elif bit == 1:
        temp[1, 3] = abs_minus(temp[1, 3])
        temp[3, 1] = abs_plus(temp[3, 1])
    return temp


def add_message(img, message):
    n = 8
    img_ = img.copy()
    pixels = img_[:, :, 2]
    w, h = np.shape(pixels)
    w -= w % n
    h -= h % n
    pixels = pixels[:w, :h]
    blocks = view_as_blocks(pixels, block_shape=(n, n))
    for i, bit in enumerate(message):
        row, column = i // blocks.shape[1], i % blocks.shape[1]
        block = blocks[row, column]
        patch = block.copy()
        flag = False
        coeff = dct(dct(patch, axis=0, norm='ortho'), axis=1, norm='ortho')
        while not flag or (bit != get_bit(block)):
            coeff = update_coeff(coeff, bit)
            diff = abs(coeff[1, 3]) - abs(coeff[3, 1])
            flag = (bit == 0 and diff > 50) or (bit == 1 and diff < -50)
            block = idct(idct(coeff, axis=0, norm='ortho'), axis=1, norm='ortho')
            res = np.uint8(np.round(np.clip(block, 0, 255), 0))
        pixels[row * n: (row + 1) * n, column * n: (column + 1) * n] = res
    img_[:w, :h, 2] = pixels
    return img_


def get_bit(block):
    row_dct = dct(block, axis=0)
    patch = dct(row_dct, axis=1)
    diff = abs(patch[1, 3]) - abs(patch[3, 1])
    return 0 if diff > 0 else 1


def hide_message(mess, img):
    array = []
    for char in mess:
        array.extend([int(x) for x in list(format(ord(char), '08b'))])
    return add_message(io.imread(img), array), len(array)


def get_message(n, img):
    img = io.imread(img)
    extracted_message = extract_message(img, n)
    mess = []
    for i in range(len(extracted_message) // 8):
        mess.append(chr(int(''.join(map(str, extracted_message[i * 8:(i + 1) * 8])), 2)))
    return ''.join(mess)


def extract_message(img, len):
    n = 8
    img = img[:, :, 2]
    w, h = np.shape(img)
    w -= w % n
    h -= h % n
    img = img[:w, :h]
    blocks = view_as_blocks(img, block_shape=(n, n))
    h = blocks.shape[1]
    return [get_bit(blocks[index // h, index % h]) for index in range(len)]


def main():
    img = "spongebob.png"
    output_img = "spongebob_output.png"
    data = "data"
    print("Input message: ", data)
    output_img_file, text_len = hide_message(data, img)
    io.imsave(output_img, output_img_file)
    print("Output message: ", get_message(text_len, output_img))


if __name__ == "__main__":
    main()
