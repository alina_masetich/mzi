import hashlib
import hmac
import math

SIZE = 64
ROTATE = [7, 12, 17, 22, 7, 12, 17, 22,
          7, 12, 17, 22, 7, 12, 17, 22,
          5, 9, 14, 20, 5, 9, 14, 20,
          5, 9, 14, 20, 5, 9, 14, 20,
          4, 11, 16, 23, 4, 11, 16, 23,
          4, 11, 16, 23, 4, 11, 16, 23,
          6, 10, 15, 21, 6, 10, 15, 21,
          6, 10, 15, 21, 6, 10, 15, 21]


def get_f(b, c, d, i):
    return {
        i < 16: (b & c) | (~b & d),
        16 <= i < 32: (d & b) | (~d & c),
        32 <= i < 48: (b ^ c ^ d),
        i >= 48: (c ^ (b | ~d))
    }[True]


def get_i(i):
    return {
        i < 16: i,
        16 <= i < 32: (5 * i + 1) % 16,
        32 <= i < 48: (3 * i + 5) % 16,
        i >= 48: (7 * i) % 16
    }[True]


def get_abcd(hash_pieces, chunk):
    T = [int(abs(math.sin(i + 1)) * 2 ** 32) & 0xFFFFFFFF for i in range(64)]
    a, b, c, d = hash_pieces
    for i in range(64):
        f = get_f(b, c, d, i)
        g = get_i(i)
        M = int.from_bytes(chunk[4 * g:4 * g + 4], byteorder='little')
        to_rotate = a + f + T[i] + M
        to_rotate &= 0xFFFFFFFF
        temp = ((to_rotate << ROTATE[i]) | (to_rotate >> (32 - ROTATE[i]))) & 0xFFFFFFFF
        new_b = b + temp
        a, b, c, d = d, new_b, b, c
    return [a, b, c, d]


def get_hash_pieces(message):
    pieces = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476]
    for chunk_ofst in range(0, len(message), 64):
        for i, val in enumerate(get_abcd(pieces, message[chunk_ofst:chunk_ofst + 64])):
            pieces[i] += val
            pieces[i] %= 4294967296
    return pieces


def md5_(message):
    message = bytearray(message)
    orig_len_in_bits = (8 * len(message))
    message.append(0x80)
    while len(message) % 64 != 56:
        message.append(0)
    message += orig_len_in_bits.to_bytes(8, byteorder='little')
    res = sum(x << (32 * i) for i, x in enumerate(get_hash_pieces(message)))
    return res


def hmac_md5(key, message):
    message = bytes(message)
    key = bytes(key)
    if len(key) > SIZE:
        key = int.to_bytes(md5_(key), SIZE, byteorder="little").rstrip(b'\0')
    key = key.ljust(SIZE, b'\0')
    opad = bytes(b'\x5c' * SIZE)
    ipad = bytes(b'\x36' * SIZE)
    o_key_pad = bytes(a ^ b for (a, b) in zip(key, opad))
    i_key_pad = bytes(a ^ b for (a, b) in zip(key, ipad))
    ikey_msg_hash = md5_(i_key_pad + message)
    inner_hash = int.to_bytes(ikey_msg_hash, SIZE, byteorder="little").rstrip(b'\0')
    outer_hash = md5_(o_key_pad + inner_hash)
    raw = outer_hash.to_bytes(16, byteorder='little')
    return '{:032x}'.format(int.from_bytes(raw, byteorder='big'))


if __name__ == "__main__":
    key = b"qqqqqqqqqwwwwwwwwwwwweeeeeeerrrrrrrtttttyyyyyyy"
    msg = b"qqqqqqqqqq"
    print(hmac_md5(key, msg))
    print(hmac.new(key=key, msg=msg, digestmod=hashlib.md5).hexdigest())

