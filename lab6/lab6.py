from codecs import getdecoder, getencoder
from os import urandom
from hashlib import md5

SIZE = 64
CURVE_P = getdecoder("hex")("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD97")[0]
CURVE_Q = getdecoder("hex")("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C611070995AD10045841B09B761B893")[0]
CURVE_A = getdecoder("hex")("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD94")[0]
CURVE_B = getdecoder("hex")("00000000000000000000000000000000000000000000000000000000000000a6")[0]
CURVE_X = getdecoder("hex")("0000000000000000000000000000000000000000000000000000000000000001")[0]
CURVE_Y = getdecoder("hex")("8D91E471E0989CDA27DF505A453F2B7635294F2DDF23E3B122ACC99C9E9F1E14")[0]


bytes2long = lambda x: int(getencoder("hex")(x)[0].decode("ascii"), 16)


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    g, y, x = egcd(b % a, a)
    return g, x - (b // a) * y, y


def mod_invert(a, m):
    g, x, y = egcd(a, m)
    return x % m


def sign(curve, key, h):
    n = curve.q
    E = bytes2long(h) % n
    if E == 0:
        E = 1
    while True:
        K = bytes2long(urandom(SIZE)) % n
        if K == 0: continue
        x_c, y_c = curve.exp(K)
        R = x_c % n
        if R == 0: continue
        S = ((R * key) + (K * E)) % n
        if S == 0: continue
        break
    return S.to_bytes(SIZE, "big") + R.to_bytes(SIZE, "big")


def check(curve, key, h, signature):
    n, S, R = curve.q, bytes2long(signature[:SIZE]), bytes2long(signature[SIZE:])
    if R <= 0 or R >= n or S <= 0 or S >= n: return False
    E = bytes2long(h) % curve.q
    if E == 0: E = 1
    V = mod_invert(E, n)
    z1 = S * V % n
    z2 = n - R * V % n
    p1x, p1y = curve.exp(z1)
    q1x, q1y = curve.exp(z2, key[0], key[1])
    x_c, y_c = curve.add(p1x, p1y, q1x, q1y)
    return x_c == R


class Curve:
    def __init__(self):
        self.p = bytes2long(CURVE_P)
        self.q = bytes2long(CURVE_Q)
        self.a = bytes2long(CURVE_A)
        self.b = bytes2long(CURVE_B)
        self.x = bytes2long(CURVE_X)
        self.y = bytes2long(CURVE_Y)

    def _pos(self, v):
        if v < 0:
            return v + self.p
        return v

    def add(self, p1x, p1y, p2x, p2y):
        if p1x == p2x and p1y == p2y:
            t = ((3 * p1x * p1x + self.a) * mod_invert(2 * p1y, self.p)) % self.p
        else:
            tx = self._pos(p2x - p1x) % self.p
            ty = self._pos(p2y - p1y) % self.p
            t = (ty * mod_invert(tx, self.p)) % self.p
        tx = self._pos(t * t - p1x - p2x) % self.p
        ty = self._pos(t * (p1x - tx) - p1y) % self.p
        return tx, ty

    def exp(self, degree, x=None, y=None):
        x = x or self.x
        y = y or self.y
        tx = x
        ty = y
        degree -= 1
        while degree != 0:
            if degree & 1 == 1:
                tx, ty = self.add(tx, ty, x, y)
            degree = degree >> 1
            x, y = self.add(x, y, x, y)
        return tx, ty


if __name__ == '__main__':
    data = 'qqqqqqqqwwwwwwweeeeeeeerrrrrrrtttttttyyyyyyyy'
    h = md5(data.encode('utf-8')).digest()
    curve = Curve()
    private_key = bytes2long(urandom(32))
    public_key = curve.exp(private_key)
    signature = sign(curve, private_key, h)
    print(check(curve, public_key, h, signature))
